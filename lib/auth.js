var $fh = require('fh-mbaas-api');

var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');

var request = require('request-promise');

var STS_SERVICE_URL = process.env.STS_SERVICE_URL || "https://demo2703148.mockable.io/token";
var IAM_AUTHZ_SERVICE_URL = process.env.IAM_AUTHZ_SERVICE_URL || "https://demo2703148.mockable.io/subject";

function retrieveToken(username, password) {
    if (typeof username !== 'undefined' && username.length > 0 &&
        typeof password !== 'undefined' && password.length > 0) {
        var options = {
            method: 'POST',
            uri: STS_SERVICE_URL,
            headers: {
                'Authorization': Buffer.from(username + ":" + password).toString('base64')
            },
            body: {
                uid: username
            },
            json: true
        };

        console.log('options', JSON.stringify(options));

        return request(options);
    }
    return new Promise(function (resolve, reject) {
        reject({status:'error', statusCode: 500, msg: 'Neither username nor password can be null or empty'});
    })
}

function retrieveSubject(uid) {
    if (typeof uid !== 'undefined' && uid.length > 0) {
        var options = {
            method: 'GET',
            uri: IAM_AUTHZ_SERVICE_URL + '/' + uid,
            json: true
        };

        console.log('options', JSON.stringify(options));

        return request(options);
    }
    return new Promise(function (resolve, reject) {
        reject({status:'error', statusCode: 500,  msg: 'Neither uid nor token can be null or empty'});
    })
}

function route() {
  var router = new express.Router();
  router.use(cors());
  router.use(bodyParser());

  router.post('/', function(req, res) {
    // Must pass a username & password
    var username = req.body.username || req.body.userId;
    var password = req.body.password;
    if (!username || !password) {
      res.status(500).json({status:'error', statusCode: 500, message: 'You need to provide a username and password.'});
      return;
    }

    let result = {};
    retrieveToken(username, password)
    .then(function (token){
        result.sessionToken = token;
        return retrieveSubject(username);
    })
    .then(function (subject){
        console.log('subject:', subject);

        result.status = 'ok';
        result.statusCode = 200;
        result.message = 'Successfully Authenticated';
        result.uid = subject.uid;
        result.authorities = subject.authorities;
        result.regid = subject.regid;

        console.log('result >>>', result);

        return res.status(200).json(result);
    })
    .catch(function (err) {
        console.log('username', username, 'not authenticated', 'err', err)
        res.status(err.statusCode).json(err);
        return;
    });
  });

  return router;
}

module.exports = route;
